#!/bin/bash

#PBS -S /bin/bash
#PBS -q normal
#PBS -l ncpus=2
#PBS -l jobfs=20GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=20GB
#PBS -l walltime=48:00:00
#PBS -N C2_H48
#PBS -P fp0

cd /g/data/wb00/admin/staging/ImageNet/ILSVRC2012/
chmod -R 775 *
