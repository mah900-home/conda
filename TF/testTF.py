#!/usr/bin/python

import tensorflow as tf

print ("tf.__version__:", tf.__version__)
#print ("tf.test.is_gpu_available() : ", tf.test.is_gpu_available())
print ("tf.test.is_built_with_cuda() : ", tf.test.is_built_with_cuda())
#print ("tf.__version__:", tf.__version__)
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
