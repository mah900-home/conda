#!/bin/bash

#PBS -S /bin/bash
#PBS -q gpuvolta
#PBS -l ncpus=768
#PBS -l ngpus=64
#PBS -l jobfs=400GB
#PBS -l storage=scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00
#PBS -l mem=1500GB
#PBS -l walltime=00:30:00
#PBS -N G64_H0.5
#PBS -P fp0

# cd  "/scratch/fp0/mah900/software2/"
# /scratch/fp0/mah900/software2/tensorflow_benchmark/setup1_pip.sh
