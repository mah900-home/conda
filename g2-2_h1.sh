#!/bin/bash

#PBS -S /bin/bash
#PBS -q gpuvolta
#PBS -l ncpus=96
#PBS -l ngpus=8
#PBS -l jobfs=10GB
#PBS -l storage=scratch/fp0
#PBS -l mem=10GB
#PBS -l walltime=01:00:00
#PBS -N GPU8_1H
#PBS -P fp0

cd  "/scratch/fp0/mah900/"
#/scratch/fp0/mah900/software2/tensorflow_benchmark/setup1_pip.sh
